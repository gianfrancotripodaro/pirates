using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola2 : MonoBehaviour
{
    public float speed;
    //public GameObject TxtDisp;
    public float cont;
    public int contTach;

    //public static bool bolaExiste2;
    void Start()
    {
        transform.Rotate(new Vector3(0, -180, 0));
        //TxtDisp.SetActive(false);
        //bolaExiste2 = true;
    }

    void Update()
    {
        destroyBola();

        Giroscopio();
    }


    public void Giroscopio()
    {

        Vector3 dir = Vector3.zero;
        //dir.y = Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 0.01)
        {
            dir.Normalize();

            dir *= Time.deltaTime;
            transform.Translate(dir * speed, Space.Self);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("AWA"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Team1"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam1--;
            }

        }

        if (collision.gameObject.CompareTag("Team2"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam2--;
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AWA"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Team1"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam1--;
            }
        }

        if (other.gameObject.CompareTag("Team2"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam2--;
            }
        }
    }

    public void destroyBola()
    {
        cont += Time.deltaTime;
        if (cont > 5)
        {
           
            Destroy(gameObject);
            cont = 0;
        }
    }
}
