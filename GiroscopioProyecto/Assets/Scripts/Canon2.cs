using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon2 : MonoBehaviour
{
    public static float velApuntado2;

    void Start()
    {
        velApuntado2 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        GirarCanon();
    }


    public void GirarCanon()
    {
        Vector3 dir = Vector3.zero;
        dir.y = -Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 0.01)
        {
            transform.Rotate(new Vector3(Input.acceleration.y, Input.acceleration.x, 0) * Time.deltaTime * velApuntado2);
        }
    }
}
