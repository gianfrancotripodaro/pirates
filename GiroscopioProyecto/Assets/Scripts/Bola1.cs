using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bola1 : MonoBehaviour
{

    public bool isFlat = true;
    public float speed;
    public float cont;
    //public GameObject TxtDisp;

    public int contTach;


    //public GameObject Credits;
   
    // Start is called before the first frame update
    void Start()
    {
        //bolaExiste = true;
        //TxtDisp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        destroyBola();
        Giroscopio();
    }


    public void Giroscopio()
    {

        Vector3 dir = Vector3.zero;
        //dir.y = Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 0.01)
        {
            dir.Normalize();

            dir *= Time.deltaTime;
            transform.Translate(dir * speed, Space.World);
        }
    }
   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("AWA"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Team1"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam1--;
            }

        }

        if (collision.gameObject.CompareTag("Team2"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam2--;
            }
        
        }

        if (collision.gameObject.CompareTag("Play"))
        {
            LevelLoader.LoadLevel("Mapa");
            

        }
        if (collision.gameObject.CompareTag("Exit"))
        {
            Application.Quit();

        }
        if (collision.gameObject.CompareTag("Credits"))
        {

            //Credits.SetActive(true);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AWA"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Team1"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam1--;
            }
        }

        if (other.gameObject.CompareTag("Team2"))
        {
            contTach++;
            if (contTach < 2)
            {
                GameManager.ContMuertesTeam2--;
            }
        }
    }

    public void destroyBola()
    {
        contTach = 0;
        cont += Time.deltaTime;
        if (cont > 5)
        {
           
            Destroy(gameObject);
            cont = 0;
        }
    }


}
