using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giroscopio : MonoBehaviour
{
    public GameObject VrCamaras;

    private float posicionInicialY = 0f;
    private float posicionDelGiroEnY = 0f;
    private float CalibrarEnLaPosicionY = 0f;

    public bool SeInicioElJuego;

    private void Start()
    {
        Input.gyro.enabled = true;
        posicionInicialY = VrCamaras.transform.eulerAngles.y;
    }

    void Update()
    {
        AplicarRotacionDelGiroscopio();
        AplicarCalibracion();


        if (SeInicioElJuego==true)
        {
            Invoke("CalibrarEnPosicionY", 3f);
            SeInicioElJuego = false;
        }
    }

    void AplicarRotacionDelGiroscopio()
    {
        VrCamaras.transform.rotation = Input.gyro.attitude;
        VrCamaras.transform.Rotate(0f, 0f, 180f, Space.Self);
        VrCamaras.transform.Rotate(90f, 180f, 0f, Space.World);
        posicionDelGiroEnY= VrCamaras.transform.eulerAngles.y;
    }

    void CalibrarEnPosicionY()
    {
        CalibrarEnLaPosicionY = posicionDelGiroEnY - posicionInicialY;
    }

    void AplicarCalibracion()
    {
        VrCamaras.transform.Rotate(0f, -CalibrarEnLaPosicionY, 0f, Space.World);
    }
}
