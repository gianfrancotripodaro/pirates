using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCannon : MonoBehaviour
{
    public float velDisparo;
    public float velApuntado2;
    public GameObject SonidoDisparo;
    public Transform disparador2;
    private Rigidbody balaImpulso;
    public Rigidbody balaPrefab2;

    public GameObject DispararBoton;

    public GameObject camaraApuntado1;


    public bool apuntando;

    void Start()
    {
        velApuntado2 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Disparar();
        GirarCanon();
    }


    public void Apuntar()
    {
        SonidoDisparo.SetActive(false);
        switch (apuntando)
        {
            case true:
                //GirarCanon();
                DispararBoton.SetActive(false);
                camaraApuntado1.SetActive(false);
                //camaraApuntado2.SetActive(false);
                apuntando = false;
                break;
            case false:
                DispararBoton.SetActive(true);
                //ApuntarBoton.SetActive(fal)
                camaraApuntado1.SetActive(true);
                //if (GameManager.TurnoP1)
                //{
                //    camaraApuntado1.SetActive(true);
                //}
                //else
                //{
                //    camaraApuntado2.SetActive(true);
                //}
                apuntando = true;
                break;
        }
        velApuntado2 = 80;
        //Canon2.velApuntado2 = 80;
    }

    public void Disparar()
    {
       
        
            SonidoDisparo.SetActive(true);
            //instancia el prefab en una posicion determinada
            balaImpulso = Instantiate(balaPrefab2, disparador2.position, Quaternion.identity);
            //le a�ade una fuerza al prefab para que sea disparado
            balaImpulso.AddForce(disparador2.forward * 100 * velDisparo * Time.deltaTime);
        
        
    }

    public void GirarCanon()
    {
        Vector3 dir = Vector3.zero;
        dir.y = -Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 0.01)
        {
          
            transform.Rotate(new Vector3(Input.acceleration.y, Input.acceleration.x, 0) * Time.deltaTime * velApuntado2);
        }
    }
}
