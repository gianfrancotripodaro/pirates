using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PantallaDeCarga : MonoBehaviour
{
 
    void Start()
    {
        string leveload = LevelLoader.nextLevel;

        StartCoroutine(this.MakeTheLoad(leveload));
    }

    IEnumerator MakeTheLoad(string level)
    {

        AsyncOperation operation = SceneManager.LoadSceneAsync(level);

       while(operation.isDone == false)
        {
            yield return null;
        }
    }
  
}
