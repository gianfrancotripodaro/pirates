using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canon : MonoBehaviour
{
    public float velDisparo;

    public Rigidbody balaPrefab1;
    public Rigidbody balaPrefab2;

    public Transform disparador1;
    public Transform disparador2;

    private Rigidbody balaImpulso;

    public GameObject camaraApuntado1;
    public GameObject camaraApuntado2;

    public static bool apuntando;
    public float VelApuntado;
    public GameObject DispararBoton;
    public GameObject SonidoDisparo;
    //public GameObject ApuntarBoton;

    //public Transform target;
    


    void Start()
    {
        DispararBoton.SetActive(false);
        camaraApuntado1.SetActive(false);
        camaraApuntado2.SetActive(false);
        VelApuntado = 0;
        SonidoDisparo.SetActive(false);
    }


    void Update()
    {
        //BloquearRotacionEnEsperaCañon();

        //Disparar();
        //Apuntar();
        GirarCanon();
        
    }


    public void Disparar()
    {
        SonidoDisparo.SetActive(true);
       
      
        if (GameManager.TurnoP1)
        {
            balaImpulso = Instantiate(balaPrefab1, disparador1.position, Quaternion.identity);
          
            balaImpulso.AddForce(disparador1.forward * 100 * velDisparo * Time.deltaTime);
        }
        else
        {
            //instancia el prefab en una posicion determinada
            balaImpulso = Instantiate(balaPrefab2, disparador2.position, Quaternion.identity);
            //le añade una fuerza al prefab para que sea disparado
            balaImpulso.AddForce(disparador2.forward * 100 * velDisparo * Time.deltaTime);
        }

        apuntando = false;
        camaraApuntado1.SetActive(false);
        camaraApuntado2.SetActive(false);
        if (GameManager.TurnoP1)
        {
            GameManager.TurnoP1 = false;
            
        }
        else
        {

            GameManager.TurnoP1 = true;
            
        }
        GameManager.ContRondas++;
        DispararBoton.SetActive(false);
        VelApuntado = 0;
        Canon2.velApuntado2 = 0; 
    }

    public void Apuntar()
    {
        SonidoDisparo.SetActive(false);
        switch (apuntando)
            {
                case true:
                    //GirarCanon();
                    DispararBoton.SetActive(false);
                    camaraApuntado1.SetActive(false);
                    camaraApuntado2.SetActive(false);
                apuntando = false;
                    break;
                case false:
                    DispararBoton.SetActive(true);
                //ApuntarBoton.SetActive(fal)

                if (GameManager.TurnoP1)
                {
                    camaraApuntado1.SetActive(true);
                }
                else
                {
                    camaraApuntado2.SetActive(true);
                }
                apuntando = true;
                    break;
            }
        VelApuntado = 80;
        Canon2.velApuntado2 = 80;
    }

    public void GirarCanon()
    {
        Vector3 dir = Vector3.zero;
        dir.y = -Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 0.01)
        {
            transform.Rotate(new Vector3(Input.acceleration.y, Input.acceleration.x, 0) * Time.deltaTime * VelApuntado);
        }
        
    }
}
