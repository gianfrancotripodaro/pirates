using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxMov : MonoBehaviour
{
    public float VelRot;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * VelRot);
    }
}
