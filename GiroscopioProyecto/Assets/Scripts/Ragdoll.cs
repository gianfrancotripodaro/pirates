using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public static bool dead;
    
    private Rigidbody[] rigidbodies;

    public GameObject cabeza1;
    public GameObject cabeza2;
    public GameObject cabeza3;
    public GameObject cabeza4;
    public GameObject cabeza5;
    public GameObject cabeza6;
    public GameObject cabeza7;
    public GameObject cabeza8;

    public GameObject bailarin;

    public GameObject SonidoMorido;

    void Start()
    {
        rigidbodies = transform.GetComponentsInChildren<Rigidbody>();
        SetEnabled(false);
        bailarin.SetActive(true);
        cabeza1.SetActive(false); cabeza2.SetActive(false); cabeza3.SetActive(false); cabeza4.SetActive(false); cabeza5.SetActive(false); cabeza6.SetActive(false); cabeza7.SetActive(false); cabeza8.SetActive(false);
    }

    void SetEnabled(bool enabled)
    {
        bool isKinematic = !enabled;
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = isKinematic;
        }

        animator.enabled = !enabled;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetEnabled(true);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            SetEnabled(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            SonidoMorido.SetActive(true);
            SetEnabled(true);
            cabeza1.SetActive(true); cabeza2.SetActive(true); cabeza3.SetActive(true); cabeza4.SetActive(true); cabeza5.SetActive(true); cabeza6.SetActive(true); cabeza7.SetActive(true); cabeza8.SetActive(true);
            bailarin.SetActive(false);
            dead = true;
            Destroy(this.gameObject,3);
            Destroy(other.gameObject,1f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            SonidoMorido.SetActive(false);
        }
    }
        
}
