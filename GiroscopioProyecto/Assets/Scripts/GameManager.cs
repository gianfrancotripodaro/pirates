using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public GameObject CamP1;
    public GameObject CamP2;

    public GameObject CanonCamP1;
    public GameObject CanonCamP2;

    public static int ContRondas;
    public static bool TurnoP1;

    public GameObject ApuntarBoton;
    public GameObject Bola;
    //public GameObject Bola2;

    //public GameObject[] Team1;
    //public GameObject[] Team2;
    //public Camera CCP1;
    //public Camera CCP2;

    public static int ContMuertesTeam1;
    public static int ContMuertesTeam2;

    [SerializeField] private Animator anim1;
    [SerializeField] private Animator anim2;

    public GameObject txtWinP1;
    public GameObject txtWinP2;

    public TMP_Text TxtTeam1;
    public TMP_Text TxtTeam2;

    public GameObject BlackTeamTurn;
    public GameObject WhiteTeamTurn;

   

    void Start()
    {
        //CamP2.SetActive(false);
        //CanonCamP2.SetActive(false);
        TurnoP1 = true;
        ContMuertesTeam1 = 5;
        ContMuertesTeam2 = 5;
        anim1.enabled = !enabled;
        anim2.enabled = !enabled;
        txtWinP2.SetActive(false);
        txtWinP1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        contarBajas();
        GestionTurnos();
        CondicionDeVictoria();
        //AvisoTurno();
        //if (!CanonCamP1)
        //{
        //    CanonCamP2.SetActive(true);
        //}
        //else
        //{
        //    CanonCamP2.SetActive(false);
        //}
    }

    public void GestionTurnos()
    {
        switch (TurnoP1)
        {
            case true:
                Debug.Log("Player 1 Turno");
                CamP1.SetActive(true);
                CamP2.SetActive(false);
                BlackTeamTurn.SetActive(true);
                WhiteTeamTurn.SetActive(false);
                break;

            case false:
                Debug.Log("Player 2 Turno");
                CamP2.SetActive(true);
                CamP1.SetActive(false);
                WhiteTeamTurn.SetActive(true);
                BlackTeamTurn.SetActive(false);
                break;
        }
    }

    public void CondicionDeVictoria()
    {
        if (ContMuertesTeam1 < 1)
        {
            Debug.Log("gana team 2");
            anim2.enabled = enabled;
            CamP1.SetActive(false);
            CamP2.SetActive(true);
            txtWinP2.SetActive(true);
        }
        else if (ContMuertesTeam2 < 1)
        {
            Debug.Log("gana team 1");
            anim1.enabled = enabled;
            CamP2.SetActive(false);
            CamP1.SetActive(true);
            txtWinP1.SetActive(true);
        }
    }

    public void contarBajas()
    {
        TxtTeam1.text = "Sailors alive: " + ContMuertesTeam1;
        TxtTeam2.text = "Sailors alive: " + ContMuertesTeam2;
    }

    //public void AvisoTurno()
    //{
    //    if (CamP1.activeSelf && !Bola1.bolaExiste)
    //    {
    //        BlackTeamTurn.SetActive(true);
    //        WhiteTeamTurn.SetActive(false);
    //    }
    //    else if(CamP2.activeSelf && !Bola2.bolaExiste2)
    //    {
    //        WhiteTeamTurn.SetActive(true);
    //        BlackTeamTurn.SetActive(false);
    //    }

    //    //if(CamP2)
    //    //{
    //    //    WhiteTeamTurn.SetActive(true);
    //    //    BlackTeamTurn.SetActive(false);
    //    //}

    //}


    public void Home()
    {
        LevelLoader.LoadLevel("Menu");
    }

}
